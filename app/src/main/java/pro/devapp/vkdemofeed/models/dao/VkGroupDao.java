package pro.devapp.vkdemofeed.models.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import pro.devapp.vkdemofeed.models.VkGroup;

@Dao
public interface VkGroupDao {

    @Query("SELECT * FROM vk_groups")
    Flowable<List<VkGroup>> getAll();

    @Insert
    long inset(VkGroup group);

    @Update
    int update(VkGroup group);

    @Delete
    int delete(VkGroup group);
    // Observable.just(comment).subscribeOn(Schedulers.io()).map({ comment1 -> commentdb.commentDao().delete(comment1) }).subscribe()

}
