package pro.devapp.vkdemofeed.models;

public class VkAttachment {
    private String type;
    private VkPhoto photo;

    public VkAttachment(String type, VkPhoto photo) {
        this.type = type;
        this.photo = photo;
    }

    public String getType() {
        return type;
    }

    public VkPhoto getPhoto() {
        return photo;
    }
}

