package pro.devapp.vkdemofeed.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "vk_groups")
public class VkGroup {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String name;

    public String image;

    public String screen_name;

    public String photo_100;

    public int is_closed;

    public long lastUpdate = 0;
}
