package pro.devapp.vkdemofeed.models;

import java.util.ArrayList;

public class VkPost {
    private long id;
    private String post_type;
    private String text;
    private long date;
    private ArrayList<VkAttachment> attachments;

    public VkPost(long id, String post_type, String text, long date, String photo_800, String photo_1280, ArrayList<VkAttachment> attachments) {
        this.id = id;
        this.post_type = post_type;
        this.text = text;
        this.date = date;
        this.attachments = attachments;
    }
    public long getId() {
        return id;
    }

    public String getPostType() {
        return post_type;
    }

    public String getPostText() {
        return text;
    }

    public long getPostDate() {
        return date;
    }

    public boolean hasPhoto() {
        if (attachments != null && attachments.size() > 0) {
            return attachments.get(0).getType().equals("photo");
        }
        return false;
    }

    public String getPhoto() {
        if (hasPhoto()) {
            return attachments.get(0).getPhoto().getPhoto_1280();
        } else {
            return null;
        }
    }
}

