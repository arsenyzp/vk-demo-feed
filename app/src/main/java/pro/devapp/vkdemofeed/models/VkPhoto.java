package pro.devapp.vkdemofeed.models;

public class VkPhoto {
    private long id;
    private long album_id;
    private long owner_id;
    private long user_id;
    private String photo_75;
    private String photo_130;
    private String photo_604;
    private String photo_807;
    private String photo_1280;
    private int width;
    private int height;
    private String text;
    private long date;
    private String access_key;

    public VkPhoto(long id, long album_id, long owner_id, long user_id, String photo_75, String photo_130, String photo_604, String photo_807, String photo_1280, int width, int height, String text, long date, String access_key) {
        this.id = id;
        this.album_id = album_id;
        this.owner_id = owner_id;
        this.user_id = user_id;
        this.photo_75 = photo_75;
        this.photo_130 = photo_130;
        this.photo_604 = photo_604;
        this.photo_807 = photo_807;
        this.photo_1280 = photo_1280;
        this.width = width;
        this.height = height;
        this.text = text;
        this.date = date;
        this.access_key = access_key;
    }

    public String getPhoto_75() {
        return photo_75;
    }

    public String getPhoto_130() {
        return photo_130;
    }

    public String getPhoto_604() {
        return photo_604;
    }

    public String getPhoto_807() {
        return photo_807;
    }

    public String getPhoto_1280() {
        return photo_1280;
    }
}
