package pro.devapp.vkdemofeed.repositories;

import java.util.ArrayList;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pro.devapp.vkdemofeed.components.VkClient;
import pro.devapp.vkdemofeed.models.VkPost;

public class VkPostRepository {
    public Observable<ArrayList<VkPost>> loadPosts(long id, int offset, int limit) {
        VkClient vk = new VkClient();
        return vk
                .getVkPosts(id * -1, offset, limit )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
