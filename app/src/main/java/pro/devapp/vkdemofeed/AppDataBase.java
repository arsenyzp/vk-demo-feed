package pro.devapp.vkdemofeed;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.models.dao.VkGroupDao;

@Database(entities = {VkGroup.class}, version = 5)
public abstract class AppDataBase extends RoomDatabase {

    private static AppDataBase instance;

    public abstract VkGroupDao vkGroupDao();

    public static synchronized AppDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class, "app_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
