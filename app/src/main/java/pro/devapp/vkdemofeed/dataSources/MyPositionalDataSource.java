package pro.devapp.vkdemofeed.dataSources;

import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.models.VkPost;
import pro.devapp.vkdemofeed.repositories.VkPostRepository;

public class MyPositionalDataSource extends PositionalDataSource<VkPost> {
    private VkGroup vkGroup;
    public MyPositionalDataSource(VkGroup vkGroup) {
        this.vkGroup = vkGroup;
    }
    @Override
    public void loadInitial(@NonNull final LoadInitialParams params, @NonNull final LoadInitialCallback<VkPost> callback) {
        VkPostRepository vkPostRepository = new VkPostRepository();
        vkPostRepository
                .loadPosts(vkGroup.id, params.requestedStartPosition, params.requestedLoadSize)
                .subscribe(new Observer<ArrayList<VkPost>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<VkPost> vkPosts) {
                        callback.onResult(vkPosts, 0);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void loadRange(@NonNull final LoadRangeParams params, @NonNull final LoadRangeCallback<VkPost> callback) {
        VkPostRepository vkPostRepository = new VkPostRepository();
        vkPostRepository
                .loadPosts(vkGroup.id, params.startPosition, params.loadSize)
                .subscribe(new Observer<ArrayList<VkPost>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<VkPost> vkPosts) {
                        callback.onResult(vkPosts);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
