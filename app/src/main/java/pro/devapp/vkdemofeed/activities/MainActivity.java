package pro.devapp.vkdemofeed.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.ActivityMainBinding;
import pro.devapp.vkdemofeed.fragments.FeedFragment;
import pro.devapp.vkdemofeed.fragments.NavigationViewFragment;
import pro.devapp.vkdemofeed.models.VkGroup;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mBinding;
    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mBinding.toolbarLayout.toolbar);

        setupDrawerToggle();
        mBinding.drawerLayout.addDrawerListener(new DrawerListener());

        fragment = FeedFragment.instantiate(this, FeedFragment.class.getName());
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    private void setupDrawerToggle() {

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbarLayout.toolbar, R.string.drawer_open, R.string.drawer_close);
        // Привязать события DrawerLayout'а к ActionBarToggle
        mBinding.drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //mBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                // Пользователь успешно авторизовался
                Log.d("VK_TEST", res.accessToken);
            }
            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void setCurrentGroup(VkGroup vkGroup) {
        ((FeedFragment)fragment).setVkGroup(vkGroup);
    }

    private class DrawerListener extends DrawerLayout.SimpleDrawerListener {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            // mBinding.relativeLayout.setTranslationX(drawerView.getWidth() * slideOffset);
        }
    }
}
