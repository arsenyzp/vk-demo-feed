package pro.devapp.vkdemofeed.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.ActivityAddGroupBinding;
import pro.devapp.vkdemofeed.viewModels.VkGroupFormViewModel;

public class AddGroupActivity extends AppCompatActivity {

    private ActivityAddGroupBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_group);

        setSupportActionBar(mBinding.toolbarLayout.toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Add group");

        mBinding.setModel(new VkGroupFormViewModel(getApplicationContext(), new VkGroupFormViewModel.EditFinishListener() {
            @Override
            public void onEndEdit() {
                finish();
            }
        }));

        mBinding.autoComplete.setThreshold(1);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
