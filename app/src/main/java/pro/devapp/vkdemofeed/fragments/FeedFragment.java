package pro.devapp.vkdemofeed.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.FragmentFeedBinding;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.viewModels.FeedListViewModel;

public class FeedFragment extends Fragment {
    private FragmentFeedBinding mBinding;
    private FeedListViewModel viewModel;
    private VkGroup vkGroup;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new FeedListViewModel();
        viewModel.onAttach(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setModel(viewModel);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (viewModel != null) {
            viewModel.onAttach(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewModel.onDetach();
    }

    public void setVkGroup(VkGroup vkGroup) {
        this.vkGroup = vkGroup;
        viewModel.setVkGroup(vkGroup);
    }
}
