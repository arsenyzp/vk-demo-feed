package pro.devapp.vkdemofeed.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.activities.MainActivity;
import pro.devapp.vkdemofeed.databinding.FragmentNavigationViewBinding;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.viewModels.NavigationViewModel;

public class NavigationViewFragment extends Fragment {

    private FragmentNavigationViewBinding mBinding;
    private RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_view, container, false);
        mBinding = DataBindingUtil.bind(view);

        layoutManager = new LinearLayoutManager(getContext());
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setLayoutManager(layoutManager);

        mBinding.setModel(new NavigationViewModel(
                getContext(),
                new NavigationViewModel.DrawerListener() {
                    @Override
                    public void closeDrawer() {
                        NavigationViewFragment.this.closeDrawer();
                    }
                },
                new NavigationViewModel.SelectGroupListener(){
                    @Override
                    public void onSelect(VkGroup vkGroup) {
                        MainActivity activity = (MainActivity) getActivity();
                        activity.setCurrentGroup(vkGroup);
                    }
                }
        ));

        return view;
    }

    private void closeDrawer() {
        // Закрыть navigation drawer
        ViewParent parent = mBinding.rootLayout.getParent();
        if (parent instanceof DrawerLayout) {
            ((DrawerLayout) parent).closeDrawers();
        } else {
            throw new IllegalStateException("Fragment should be a child of DrawerLayout for proper work");
        }
    }
}
