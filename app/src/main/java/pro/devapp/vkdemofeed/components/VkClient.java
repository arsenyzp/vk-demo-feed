package pro.devapp.vkdemofeed.components;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import pro.devapp.vkdemofeed.api.responseModels.VkBaseResponse;
import pro.devapp.vkdemofeed.api.responseModels.VkGroupsResponse;
import pro.devapp.vkdemofeed.api.responseModels.VkPostsResponse;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.models.VkPost;

public class VkClient {
    private Client clietn;

    public VkClient(){
        clietn = Client.getInstance();
    }

    public Observable<ArrayList<VkPost>> getVkPosts (long groupId, int offset, int limit) {
        return clietn.getApi().getList(groupId, "5.52", "238467162384671623846716d623cdcfa822384238467167ac760a2f5508d6baa94a087", "owner", limit, offset)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<VkBaseResponse, ArrayList<VkPost>>() {
                    @Override
                    public ArrayList<VkPost> apply(VkBaseResponse vkBaseResponse) throws Exception {
                        if (vkBaseResponse.hasError()) {
                            throw new Exception(vkBaseResponse.getErrorMessage());
                        }
                        return vkBaseResponse.getResponse(VkPostsResponse.class).getItems();
                    }
                });
    }

    public Observable<ArrayList<VkGroup>> getVkGroups (String name) {
        return clietn.getApi().findGroups(name, "group", "5.52","17baceefb8b9464c594b6c95afe0fa0428ae860226eb747f40e51540a9ff244c6d23916c1c7b0a3209b6a", 20, 0)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<VkBaseResponse, ArrayList<VkGroup>>() {
                    @Override
                    public ArrayList<VkGroup> apply(VkBaseResponse vkBaseResponse) throws Exception {
                        if (vkBaseResponse.hasError()) {
                            throw new Exception(vkBaseResponse.getErrorMessage());
                        }
                        return vkBaseResponse.getResponse(VkGroupsResponse.class).getItems();
                    }
                });
    }
}
