package pro.devapp.vkdemofeed.api.responseModels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

public class VkBaseResponse {
    private Object response;
    private VkErrorResponse error;

    VkBaseResponse(Object response) {
        this.response = response;
    }

    public boolean hasError() {
        return error != null;
    }

    public String getErrorMessage() {
        if (hasError()) {
            return  error.error_msg;
        }
        return "Server error";
    }

    public <T> T getResponse(Class<T> classOfT) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create();
        return gson.fromJson(gson.toJson(response), classOfT);
    }
}