package pro.devapp.vkdemofeed.api.responseModels;

import java.util.ArrayList;

import pro.devapp.vkdemofeed.models.VkGroup;

public class VkGroupsResponse {
    private long count;
    private ArrayList<VkGroup> items;

    VkGroupsResponse(long count, ArrayList<VkGroup> items){
        this.count = count;
        this.items = items;
    }

    public ArrayList<VkGroup> getItems() {
        return items;
    }

    public long getCount() {
        return count;
    }
}
