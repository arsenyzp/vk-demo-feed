package pro.devapp.vkdemofeed.api.responseModels;

public class VkErrorResponse {
    public int error_code;
    public String error_msg;
}