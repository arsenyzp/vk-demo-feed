package pro.devapp.vkdemofeed.api.responseModels;

import java.util.ArrayList;
import java.util.Iterator;

import pro.devapp.vkdemofeed.models.VkPost;

public class VkPostsResponse {
    private long count;
    private ArrayList<VkPost> items;

    VkPostsResponse(long count, ArrayList<VkPost> items){
        this.count = count;
        this.items = items;
    }

    public ArrayList<VkPost> getItems() {
        ArrayList<VkPost> posts = (ArrayList<VkPost>)items.clone();
        Iterator<VkPost> itpost = posts.iterator();
        ArrayList<VkPost> filteredPosts = new ArrayList<>();
        while (itpost.hasNext()) {
            VkPost post = itpost.next();
            if (!post.getPostType().equals("video")) {
                filteredPosts.add(post);
            }
        }
        return filteredPosts;
    }

    public long getCount() {
        return count;
    }
}