package pro.devapp.vkdemofeed.api;

import io.reactivex.Observable;
import pro.devapp.vkdemofeed.api.responseModels.VkBaseResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VkApi {
    @GET("wall.get")
    Observable<VkBaseResponse> getList(
            @Query("owner_id") long owner_id,
            @Query("v") String v,
            @Query("access_token") String access_token,
            @Query("filter") String filter,
            @Query("count") int count,
            @Query("offset") int offset
    );

    @GET("groups.search")
    Observable<VkBaseResponse> findGroups(
            @Query("q") String q,
            @Query("type") String type,
            @Query("v") String v,
            @Query("access_token") String access_token,
            @Query("count") int count,
            @Query("offset") int offset
    );
}

