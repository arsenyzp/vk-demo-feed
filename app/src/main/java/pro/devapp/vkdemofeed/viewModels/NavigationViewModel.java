package pro.devapp.vkdemofeed.viewModels;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.util.List;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import pro.devapp.vkdemofeed.AppDataBase;
import pro.devapp.vkdemofeed.activities.AddGroupActivity;
import pro.devapp.vkdemofeed.adapters.VkGroupListAdapter;
import pro.devapp.vkdemofeed.models.VkGroup;

public class NavigationViewModel extends BaseObservable {
    private Context context;
    private VkGroupListAdapter adapter;
    private DrawerListener drawerListener;
    private SelectGroupListener selectGroupListener;
    public ObservableField<Boolean> isGroups = new ObservableField<>(false);

    public NavigationViewModel(Context contex, DrawerListener drawerListener, SelectGroupListener selectGroupListener) {
        this.context = contex;
        this.drawerListener = drawerListener;
        this.selectGroupListener = selectGroupListener;
        this.adapter = new VkGroupListAdapter(new VkGroupListAdapter.ListItemClickListener() {
            @Override
            public void onClick(int position) {
                VkGroup vkGroup = adapter.getItem(position);
                NavigationViewModel.this.selectGroupListener.onSelect(vkGroup);
                NavigationViewModel.this.drawerListener.closeDrawer();
            }
        });
        this.loadList();
    }

    public void onAdd(View view) {
        Intent intent = new Intent();
        intent.setClass(context, AddGroupActivity.class);
        drawerListener.closeDrawer();
        context.startActivity(intent);
    }

    private Disposable loadList() {
        return AppDataBase
                .getInstance(context)
                .vkGroupDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<VkGroup>>() {
                    @Override
                    public void accept(List<VkGroup> vkGroups){
                        Log.i("VK_TEST", vkGroups.size() + "");
                        adapter.setData(vkGroups);
                        if (vkGroups.size() > 0) {
                            isGroups.set(true);
                        } else {
                            isGroups.set(false);
                        }
                    }
                });
    }

    @Bindable
    public VkGroupListAdapter getAdapter(){
        return adapter;
    }

    public interface DrawerListener {
        public void closeDrawer();
    }

    public interface SelectGroupListener {
        public void onSelect(VkGroup vkGroup);
    }
}
