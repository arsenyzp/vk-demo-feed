package pro.devapp.vkdemofeed.viewModels;

import android.arch.paging.PagedList;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.view.View;

import com.vk.sdk.VKSdk;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import pro.devapp.vkdemofeed.adapters.FeedListPagerAdapter;
import pro.devapp.vkdemofeed.dataSources.MyPositionalDataSource;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.models.VkPost;

public class FeedListViewModel {
    private boolean needAuth = false;
    private Fragment fragment;
    private FeedListPagerAdapter adapter;

    public FeedListViewModel() {
//        this.adapter = new FeedListAdapter();
        this.adapter = new FeedListPagerAdapter(new DiffUtil.ItemCallback<VkPost>() {
            @Override
            public boolean areItemsTheSame(@NonNull VkPost vkPost1, @NonNull VkPost vkPost2) {
                return vkPost1.getId() == vkPost2.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull VkPost vkPost1, @NonNull VkPost vkPost2) {
                return vkPost1.getId() == vkPost2.getId();
            }
        });
    }

    public void onAttach(Fragment fragment) {
        this.fragment = fragment;
    }

    private void loadPosts(VkGroup vkGroup) {
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(20)
                .setPrefetchDistance(5)
                .build();

        MyPositionalDataSource dataSource = new MyPositionalDataSource(vkGroup);

        PagedList<VkPost> pagedList = new PagedList.Builder<>(dataSource, config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setNotifyExecutor(new MainThreadExecutor())
                .build();

        adapter.submitList(pagedList);
    }

    public void onDetach() {
        this.fragment = null;
    }

    public void onStartVkAuth(View view) {
        if (fragment != null) {
            VKSdk.login(fragment.getActivity(), "wall");
        }
    }

    public boolean isNeedAuth() {
        return needAuth;
    }

    public void setNeedAuth(boolean needAuth) {
        this.needAuth = needAuth;
    }

    public FeedListPagerAdapter getAdapter() {
        return adapter;
    }

    static class MainThreadExecutor implements Executor {
        private final Handler mHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            mHandler.post(command);
        }
    }

    public void setVkGroup(VkGroup vkGroup) {
        loadPosts(vkGroup);
    }
}

