package pro.devapp.vkdemofeed.viewModels;

import pro.devapp.vkdemofeed.models.VkGroup;

public class VkGroupViewModel {
    VkGroup model;
    public VkGroupViewModel(VkGroup model){
        this.model = model;
    }

    public String getName() {
        return model.name;
    }

    public String getImageUrl() {
        return model.photo_100;
    }
}
