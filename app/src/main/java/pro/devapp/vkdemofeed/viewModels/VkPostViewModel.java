package pro.devapp.vkdemofeed.viewModels;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.models.VkPost;

public class VkPostViewModel {
    private VkPost post;

    public VkPostViewModel(VkPost post){
        this.post = post;
    }

    public String getText() {
        return post.getPostText();
    }

    public String getSortText() {
        if (post.getPostText().length() > 100) {
            return post.getPostText().substring(0, 100);
        }
        return post.getPostText();
    }

    public boolean hasImage() {
        return post.hasPhoto();
    }

    public String getImageUrl() {
        return post.getPhoto();
    }

    public int getDefaultImage() {
        return R.drawable.ic_vk;
    }
}

