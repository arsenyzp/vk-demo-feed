package pro.devapp.vkdemofeed.viewModels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import pro.devapp.vkdemofeed.AppDataBase;
import pro.devapp.vkdemofeed.adapters.AutoCompleateAdapter;
import pro.devapp.vkdemofeed.components.VkClient;
import pro.devapp.vkdemofeed.models.VkGroup;

public class VkGroupFormViewModel extends BaseObservable {
    private VkGroup group;
    private EditFinishListener listener;
    public ObservableField<String> errorMessage = new ObservableField<>();
    public ObservableField<String> search = new ObservableField<>();
    private Context context;
    private Disposable findGroupDisposable;
    private VkClient vk = new VkClient();
    private AutoCompleateAdapter adapter;
    private AdapterView.OnItemClickListener onItemClickListener;

    public VkGroupFormViewModel(Context context, EditFinishListener listener){
        this.listener = listener;
        this.context = context;
        this.adapter = new AutoCompleateAdapter(this.context);
        this.onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                VkGroup group = adapter.getItem(position);
                setGroup(group);
            }
        };
    }

    private void setGroup(VkGroup group) {
        this.group = group;
        search.set("");
        notifyChange();
    }

    public void onSave(View view) {
        if (validate()) {
            Single.create(new SingleOnSubscribe<Long>() {
                @Override
                public void subscribe(SingleEmitter<Long> emitter) throws Exception {
                    try {
                        long id = AppDataBase
                                .getInstance(context)
                                .vkGroupDao()
                                .inset(group);
                        emitter.onSuccess(id);
                    } catch (Exception e) {
                        emitter.onError(e);
                    }
                }
            })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new SingleObserver<Long>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Long aLong) {
                            listener.onEndEdit();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context,"Ошибка базы", Toast.LENGTH_LONG).show();
                            Log.e("DB", e.getMessage());
                        }
                    });
        }
    }

    public void onCancel(View view) {
        this.group = null;
        notifyChange();
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        VkGroup group = adapter.getItem(position);
        setGroup(group);
    }

    private boolean validate() {
        if (group == null) {
            errorMessage.set("Нужно выбрать группу");
            return false;
        }
        return true;
    }

    @Bindable
    public boolean isSelected() {
        return group != null;
    }

    @Bindable
    public String getImage() {
        if (!isSelected()) {
            return null;
        }
        return group.photo_100;
    }

    @Bindable
    public String getName() {
        if (!isSelected()) {
            return null;
        }
        return  group.name;
    }

    @Bindable
    public AutoCompleateAdapter getAdapter () {
        return adapter;
    }

    @Bindable
    public AdapterView.OnItemClickListener getOnItemClickListener () {
        return this.onItemClickListener;
    }

    public void onTextChanged(Editable editable) {
        Log.d("TEXT_WATCHER", editable.toString());
        findGroup(editable.toString());
    }

    private void findGroup(String name){
        if (findGroupDisposable != null) {
            findGroupDisposable.dispose();
        }
        if (name.equals("")) {
            return;
        }
        findGroupDisposable = vk.getVkGroups(name).subscribe(new Consumer<ArrayList<VkGroup>>() {
            @Override
            public void accept(ArrayList<VkGroup> vkGroups) {
                adapter.setData(vkGroups);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static interface EditFinishListener {
        public void onEndEdit();
    }
}