package pro.devapp.vkdemofeed;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class BindingAdapters {

    /**
     * For EditTextLayout
     * @param view
     * @param errorMessage
     */
    @BindingAdapter(value = "errorMessage")
    public static void setErrorMessage(TextInputLayout view, String errorMessage) {
        view.setError(errorMessage);
    }

    /**
     * Load images from url
     * @param view
     * @param url
     * @param targetWidth
     * @param targetHeight
     */
    @BindingAdapter(value={"imageUrl", "targetWidth", "targetHeight"}, requireAll=true)
    public static void setImageUrl(final ImageView view, final String url, final int targetWidth, final int targetHeight) {
        if (url == null) {
            return;
        }
        Picasso.get()
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .resize(targetWidth, targetHeight)
                .centerCrop()
                .into(view, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        //Try again online if cache failed
                        Picasso.get()
                                .load(url)
                                .resize(targetWidth, targetHeight)
                                .centerCrop()
                                .into(view, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Log.v("Picasso","Could not fetch image");
                                    }
                                });
                    }
                });
    }

    /**
     * Set ArrayAdapter
     * @param view
     * @param adapter
     */
    @BindingAdapter(value = "adapter")
    public static void setAdapter(android.support.v7.widget.AppCompatAutoCompleteTextView view, ArrayAdapter adapter) {
        view.setAdapter(adapter);
    }

    @BindingAdapter("onItemClickListener")
    public static void setOnItemClickListener(android.support.v7.widget.AppCompatAutoCompleteTextView view, AdapterView.OnItemClickListener listener) {
        view.setOnItemClickListener(listener);
    }

    /**
     * RecycleView
     */
    @BindingAdapter(value = "adapter")
    public static void setAdapter(android.support.v7.widget.RecyclerView view, RecyclerView.Adapter adapter) {
        view.setAdapter(adapter);
    }


}
