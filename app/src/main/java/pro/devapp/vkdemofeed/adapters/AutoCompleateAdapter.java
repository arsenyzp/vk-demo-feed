package pro.devapp.vkdemofeed.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.ItemAutoCompleateBinding;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.viewModels.VkGroupViewModel;

public class AutoCompleateAdapter extends ArrayAdapter<VkGroup> {
    private List<VkGroup> items = new ArrayList<>();

    public AutoCompleateAdapter(Context context) {
        super(context, R.layout.item_auto_compleate, new ArrayList<VkGroup>());
    }

    public void setData(List<VkGroup> data) {
        this.items.clear();
        this.items.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        ItemAutoCompleateBinding binding;
        if(listItem == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.item_auto_compleate, parent, false);
        } else {
            binding = DataBindingUtil.findBinding(convertView);
        }
        VkGroup group = getItem(position);
        binding.setGroup(new VkGroupViewModel(group));
        return binding.getRoot();
    }

    @Nullable
    @Override
    public VkGroup getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

