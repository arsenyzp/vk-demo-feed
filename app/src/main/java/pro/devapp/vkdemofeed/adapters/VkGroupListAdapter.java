package pro.devapp.vkdemofeed.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.ItemGroupBinding;
import pro.devapp.vkdemofeed.models.VkGroup;
import pro.devapp.vkdemofeed.viewModels.VkGroupViewModel;

public class VkGroupListAdapter extends RecyclerView.Adapter<VkGroupListAdapter.VkGroupHolder> {

    private List<VkGroup> items = new LinkedList<>();
    private ListItemClickListener listItemClickListener;

    public VkGroupListAdapter(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    public void setData(List<VkGroup> data) {
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VkGroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemGroupBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_group, parent, false);
        return new VkGroupHolder(binding, listItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull VkGroupHolder holder, int position) {
        holder.bind(new VkGroupViewModel(items.get(position)));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public VkGroup getItem(int position) {
        return items.get(position);
    }

    static class VkGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemGroupBinding binding;
        ListItemClickListener clickListener;

        public VkGroupHolder(ItemGroupBinding binding, ListItemClickListener clickListener){
            super(binding.getRoot());
            this.binding = binding;
            this.clickListener = clickListener;
        }

        public void bind(VkGroupViewModel viewModel) {
            binding.setGroup(viewModel);
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(this);
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            clickListener.onClick(getAdapterPosition());
        }
    }

    public interface ListItemClickListener {
        public void onClick(int position);
    }
}