package pro.devapp.vkdemofeed.adapters;

import android.arch.paging.PagedListAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import pro.devapp.vkdemofeed.R;
import pro.devapp.vkdemofeed.databinding.ItemPostBinding;
import pro.devapp.vkdemofeed.models.VkPost;
import pro.devapp.vkdemofeed.viewModels.VkPostViewModel;

public class FeedListPagerAdapter extends PagedListAdapter<VkPost, FeedListPagerAdapter.VkPostHolder> {

    public FeedListPagerAdapter(@NonNull DiffUtil.ItemCallback<VkPost> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public VkPostHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemPostBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_post, parent, false);
        return new FeedListPagerAdapter.VkPostHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VkPostHolder holder, int position) {
        holder.bind(new VkPostViewModel(getItem(position)));
    }

    static class VkPostHolder extends RecyclerView.ViewHolder{
        ItemPostBinding binding;
        public VkPostHolder(ItemPostBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(VkPostViewModel postModel) {
            binding.setModel(postModel);
            binding.executePendingBindings();
        }
    }
}
